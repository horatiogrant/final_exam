/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

/**
 *
 * @author ramses
 */
public class ShoppingCartDemo {
    
    public static void main( String args [ ] ) {
        DiscountFactory dis = DiscountFactory.discFactory();
        PaymentServiceFactory factory = PaymentServiceFactory.getInstance( );
        PaymentService creditService = factory.getPaymentService( PaymentServiceType.CREDIT );
        PaymentService debitService = factory.getPaymentService( PaymentServiceType.DEBIT );        
            // create cart and add products
        Cart cart = new Cart( );
        cart.addProduct( new Product( "shirt" , 50 ) );
        cart.addProduct( new Product( "pants" , 60 ) );
        
            //create discountAmount
      cart.setDiscount(dis.getDiscount(DiscountType.AMOUNT, 10.0));
            // set credit service and pay
        cart.setPaymentService(creditService);        
        cart.payCart();
        
        //create discountPercent
         cart.setDiscount(dis.getDiscount(DiscountType.PERCENT, 20));
            // set debit service and pay
        cart.setPaymentService( debitService );    
        cart.payCart();        
        
    } 
    
}
