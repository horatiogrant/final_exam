/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sheridan;

/**
 *
 * @author grant
 */
public abstract class Discount {
    
    double discountAmount;
    
    public Discount(double discountAmount){
        this.discountAmount=discountAmount;
    }
    public double calculateDiscount(double amount){
        return discountAmount;
    }
    
}
