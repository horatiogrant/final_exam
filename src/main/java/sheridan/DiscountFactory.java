/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sheridan;

/**
 *
 * @author grant
 */
public class DiscountFactory {
      static DiscountFactory discFac;
      private DiscountFactory(){}
    
    public static DiscountFactory discFactory(){
       
        if(discFac==null){
             discFac= new DiscountFactory(); 
        }
      
        return discFac;
    }
    
    public Discount getDiscount(DiscountType type, double amt){
     
         if(type==DiscountType.AMOUNT){
               return new DiscountByAmount(amt) ;
          }else{
           return new DiscountByPercentage(amt) ;
       }
       
    }
    
}
