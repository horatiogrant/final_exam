/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sheridan;

/**
 *
 * @author grant
 */
public class DiscountByAmount extends Discount{
  
     public DiscountByAmount(double amt){
         super(amt);
     }
    
    @Override
    public double calculateDiscount(double amount){
        double discount = amount - discountAmount;
        
       
        return discount;
    }
}
