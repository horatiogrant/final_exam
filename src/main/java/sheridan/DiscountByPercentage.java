/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sheridan;



/**
 *
 * @author grant
 */
public class DiscountByPercentage extends Discount {
  
     public DiscountByPercentage(double amt){
         super(amt);
     }
    
      @Override
    public double calculateDiscount(double amount){
        
        double discount = amount-(amount * discountAmount/100);
        
       
        return discount;
    }
}
