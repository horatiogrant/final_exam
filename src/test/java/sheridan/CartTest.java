/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package sheridan;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author grant
 */
public class CartTest {
    
    public CartTest() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    
    

    /**
     * Test of setPaymentService method, of class Cart.
     
    @Test
    public void testSetPaymentService() {
        System.out.println("setPaymentService");
        PaymentService service = null;
        Cart instance = new Cart();
        instance.setPaymentService(service);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
*/
    /**
     * Test of addProduct method, of class Cart.
     */
    @Test
    public void testAddProduct() {
        boolean expR=true, result;
        System.out.println("addProduct test");
        Product product = new Product("apple",1);
        Cart instance = new Cart();
        instance.addProduct(product);
       if (instance.getCartSize()>0){
           result=true;
       }else{
           result=false;
       }
        assertEquals(expR,result);
    }
    
     @Test
    public void testAddProductFail() {
        boolean expR=false, result;
        System.out.println("addProductFail test");
        
        Cart instance = new Cart();
        
         if (instance.getCartSize()<=0){
           result=false;
       }else{
           result=true;
       }
     assertEquals(expR,result);
    }

    /**
     * Test of payCart method, of class Cart.
     */
    /*
    @Test
    public void testPayCart() {
        System.out.println("payCart");
        Cart instance = new Cart();
        instance.payCart();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    */
}
